﻿
// ShapedMatchDlg.cpp: 实现文件
//

#include "pch.h"
#include "framework.h"
#include "ShapedMatch.h"
#include "ShapedMatchDlg.h"
#include "afxdialogex.h"
#include<sstream>
#include <atlstr.h>
#include "line2Dup.h"
//#include "line2Dup.cpp"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif




using namespace std;
using namespace cv;

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CShapedMatchDlg 对话框



CShapedMatchDlg::CShapedMatchDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_SHAPEDMATCH_DIALOG, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	
}

void CShapedMatchDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_TempPath, TempatePath);

	DDX_Control(pDX, IDC_EDIT_Score, m_ScoreMin);
	//  DDX_Control(pDX, IDC_LIST1, m_listMsg);
	DDX_Control(pDX, IDC_LIST1, m_listMsg);
	DDX_Control(pDX, IDC_STATIC_TimeMatch, m_matchTime);
}

BEGIN_MESSAGE_MAP(CShapedMatchDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON_SelTemp, &CShapedMatchDlg::OnBnClickedButtonSeltemp)
	ON_BN_CLICKED(IDC_BUTTON_Src, &CShapedMatchDlg::OnBnClickedButtonSrc)
	ON_BN_CLICKED(IDC_BUTTON2, &CShapedMatchDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON_MatchTool, &CShapedMatchDlg::OnBnClickedButtonMatchtool)
END_MESSAGE_MAP()

void CVMatToCImage(cv::Mat& cv_img, CImage& c_img, CRect dest_img_size)
{
	int cxl = 0, cyl = 0;
	int width, height;
	c_img.Destroy();
	width = dest_img_size.right;
	height = dest_img_size.bottom;

	cv::Mat frame_dest_size;
	cv::resize(cv_img, frame_dest_size, cv::Size(width, height));

	int nBPP = frame_dest_size.channels() * 8;
	c_img.Create(frame_dest_size.cols, frame_dest_size.rows, nBPP);
	if (nBPP == 8)
	{
		static RGBQUAD pRGB[256] = { 0 };
		for (int i = 0; i < 256; i++)
			pRGB[i].rgbBlue = pRGB[i].rgbGreen = pRGB[i].rgbRed = i;
		c_img.SetColorTable(0, 256, pRGB);
	}
	uchar* psrc = frame_dest_size.data;
	uchar* pdst = (uchar*)c_img.GetBits();
	int imgPitch = c_img.GetPitch();
	for (int y = 0; y < frame_dest_size.rows; y++)
	{
		memcpy(pdst, psrc, frame_dest_size.cols * frame_dest_size.channels());
		psrc += frame_dest_size.step;
		pdst += imgPitch;
	}
}
// CShapedMatchDlg 消息处理程序

void CShapedMatchDlg::ShowImage(CWnd* pWnd1, cv::Mat src_img)
{
	CImage	image2;
	CRect rect2;
	pWnd1->GetClientRect(&rect2);
	CVMatToCImage(src_img, image2, rect2);
	CDC* pDc1 = NULL;
	pDc1 = pWnd1->GetDC();
	image2.Draw(pDc1->m_hDC, rect2);
	this->ReleaseDC(pDc1);
}
#define SUBITEM_INDEX 0
#define SUBITEM_SCORE 1
#define SUBITEM_ANGLE 2
#define SUBITEM_POS_X 3
#define SUBITEM_POS_Y 4

BOOL CShapedMatchDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	TempatePath.SetWindowText("./templ.yaml");
	m_ScoreMin.SetWindowTextA("0.9");

	m_listMsg.InsertColumn(0,"序号", LVCFMT_CENTER, 40);
	m_listMsg.InsertColumn(1, "分数", LVCFMT_CENTER, 70);
	m_listMsg.InsertColumn(2, "角度", LVCFMT_CENTER, 90);
	m_listMsg.InsertColumn(3, "X", LVCFMT_CENTER, 100);
	m_listMsg.InsertColumn(4, "Y", LVCFMT_CENTER, 100);

	m_listMsg.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_INFOTIP | LVS_EX_GRIDLINES);
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CShapedMatchDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CShapedMatchDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CShapedMatchDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CShapedMatchDlg::OnBnClickedButtonSeltemp()
{
	// TODO: 在此添加控件通知处理程序代码
	int width, height;
	CImage	image;
	//创建打开图片的文件路径对话框
	CFileDialog dlg(TRUE, NULL, NULL, 0, _T("All Files (*.*)|*.*|BMP (*.bmp)|*.bmp|DIB (*.dib)|*.dib|EMF (*.emf)|*.emf|GIF (*.gif)|*.gif|ICO (*.ico)|*.ico|JPG (*.jpg)|*.jpg|WMF (*.wmf)|*.wmf||"), NULL);
	if (dlg.DoModal() == IDOK)
	{
		CString FilePathName = dlg.GetPathName();
		CBitmap bitmap;  // CBitmap对象，用于加载位图
		HBITMAP hBmp;    // 保存CBitmap加载的位图的句柄
		CRect rectl;
		//this->GetDlgItem(IDC_STATICwer)->GetWindowRect(&rectl);
		//this->ScreenToClient(&rectl);
		//ora_dlg->GetDlgItem(IDC_STATICwer)->MoveWindow(rectl.left, rectl.top, cxl, cyl, TRUE);
		image.Load(FilePathName);
		std::string s = CT2A(FilePathName.GetBuffer());
		TemplateImage = cv::imread(s);
		ShowImage(this->GetDlgItem(IDC_STATIC_Template), TemplateImage);
	}

}


void CShapedMatchDlg::OnBnClickedButtonSrc()
{
	// TODO: 在此添加控件通知处理程序代码
	// TODO: 在此添加控件通知处理程序代码
	int width, height;
	CImage	image;
	//创建打开图片的文件路径对话框
	CFileDialog dlg(TRUE, NULL, NULL, 0, _T("All Files (*.*)|*.*|BMP (*.bmp)|*.bmp|DIB (*.dib)|*.dib|EMF (*.emf)|*.emf|GIF (*.gif)|*.gif|ICO (*.ico)|*.ico|JPG (*.jpg)|*.jpg|WMF (*.wmf)|*.wmf||"), NULL);
	if (dlg.DoModal() == IDOK)
	{
		CString FilePathName = dlg.GetPathName();
		CBitmap bitmap;  // CBitmap对象，用于加载位图
		HBITMAP hBmp;    // 保存CBitmap加载的位图的句柄
		CRect rectl;
		//this->GetDlgItem(IDC_STATICwer)->GetWindowRect(&rectl);
		//this->ScreenToClient(&rectl);
		//ora_dlg->GetDlgItem(IDC_STATICwer)->MoveWindow(rectl.left, rectl.top, cxl, cyl, TRUE);
		image.Load(FilePathName);
		std::string s = CT2A(FilePathName.GetBuffer());
		SrcImage = cv::imread(s);
		ShowImage(this->GetDlgItem(IDC_STATIC_SrcImage), SrcImage);
	}
}

static std::string prefix = "C:/Users/master/Downloads/shape_based_matching_subpixel-master3/shape_based_matching_subpixel-master/shape_based_matching-subpixel/shape_based_matching-subpixel/test1/";
void CShapedMatchDlg::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码
	if (TemplateImage.empty())
	{
		AfxMessageBox("请选择模板文件");
		return;
	}
	Mat img = TemplateImage.clone();
	int minlenth = std::min(img.rows, img.cols);
	int num_features = std::max(minlenth / 3, 50);

	line2Dup::Detector detector(num_features, { 4, 8 });

	assert(!img.empty() && "check your img path");
	Mat mask = Mat(img.size(), CV_8UC1, { 255 });

	// padding to avoid rotating out
	int padding = 100;
	cv::Mat padded_img = cv::Mat(img.rows + 2 * padding, img.cols + 2 * padding, img.type(), cv::Scalar::all(0));
	img.copyTo(padded_img(Rect(padding, padding, img.cols, img.rows)));

	cv::Mat padded_mask = cv::Mat(mask.rows + 2 * padding, mask.cols + 2 * padding, mask.type(), cv::Scalar::all(0));
	mask.copyTo(padded_mask(Rect(padding, padding, img.cols, img.rows)));

	shape_based_matching::shapeInfo_producer shapes(padded_img, padded_mask);
	shapes.angle_range = { 0, 360 };
	shapes.angle_step = 0.1;
	shapes.scale_range = { 1 };
	//shapes.scale_step = 0.1f;
	shapes.produce_infos();
	std::vector<shape_based_matching::shapeInfo_producer::Info> infos_have_templ;
	string class_id = "test";
	bool is_first = true;

	// for other scales you want to re-extract points: 
	// set shapes.scale_range then produce_infos; set is_first = false;

	int first_id = 0;
	float first_angle = 0;
	bool use_rot = true;
	for (auto& info : shapes.infos) {
		Mat to_show = shapes.src_of(info);

		std::cout << "\ninfo.angle: " << info.angle << std::endl;
		int templ_id;

		if (is_first) {
			templ_id = detector.addTemplate(shapes.src_of(info), class_id, shapes.mask_of(info));
			first_id = templ_id;
			first_angle = info.angle;

			if (use_rot) is_first = false;
		}
		else {
			templ_id = detector.addTemplate_rotate(class_id, first_id,
				info.angle - first_angle,
				{ shapes.src.cols / 2.0f, shapes.src.rows / 2.0f });
		}

		auto templ = detector.getTemplates("test", templ_id);
		for (int i = 0; i < templ[0].features.size(); i++) {
			auto feat = templ[0].features[i];
			cv::circle(to_show, { feat.x + templ[0].tl_x, feat.y + templ[0].tl_y }, 3, { 0, 0, 255 }, -1);
		}

		// will be faster if not showing this
		cv::imshow("train", to_show);
		//cv::waitKey(1);
		std::cout << "templ_id: " << templ_id << std::endl;
		if (templ_id != -1) {
			infos_have_templ.push_back(info);
		}
	}
	CString TempTatePathUser;
	TempatePath.GetWindowText(TempTatePathUser);
	std::string sPath = CT2A(TempTatePathUser.GetBuffer());
	detector.writeClasses(sPath);
	//string::size_type iPos = (SaveTemplatePath.find_last_of('\\') + 1) == 0 ? SaveTemplatePath.find_last_of('/') + 1 : SaveTemplatePath.find_last_of('\\') + 1;
	//string shapeInfosPath = SaveTemplatePath.substr(0, iPos);//获取文件路径
	shapes.save_infos(infos_have_templ, sPath + "_info");
	std::cout << "train end" << std::endl << std::endl;
}

class Timer
{
public:
	Timer() : beg_(clock_::now()) {}
	void reset() { beg_ = clock_::now(); }
	double elapsed() const {
		return std::chrono::duration_cast<second_>
			(clock_::now() - beg_).count();
	}
	void out(std::string message = "") {
		double t = elapsed();
		std::cout << message << "\nelasped time:" << t << "s\n" << std::endl;
		reset();
	}
private:
	typedef std::chrono::high_resolution_clock clock_;
	typedef std::chrono::duration<double, std::ratio<1> > second_;
	std::chrono::time_point<clock_> beg_;
};


void CShapedMatchDlg::OnBnClickedButtonMatchtool()
{
	
	// TODO: 在此添加控件通知处理程序代码
	if (SrcImage.empty())
	{
		AfxMessageBox("请选择源文件");
		return;
	}
	line2Dup::Detector detector(128, { 4, 8 });
	std::vector<std::string> ids;
	ids.push_back("test");
	CString TempTatePathUser,m_ScoreStr;
	TempatePath.GetWindowText(TempTatePathUser);
	std::string sPath = CT2A(TempTatePathUser.GetBuffer());
	detector.readClasses(ids, sPath);

	m_ScoreMin.GetWindowTextA(m_ScoreStr);

	float ScoreMin_m = atof(m_ScoreStr) * 100.0f;

	// angle & scale are saved here, fetched by match id
	auto infos = shape_based_matching::shapeInfo_producer::load_infos(sPath + "_info");

	Mat test_img = SrcImage.clone();
	assert(!test_img.empty() && "check your img path");

	int padding = 250;
	cv::Mat padded_img = cv::Mat(test_img.rows + 2 * padding,
		test_img.cols + 2 * padding, test_img.type(), cv::Scalar::all(0));
	test_img.copyTo(padded_img(Rect(padding, padding, test_img.cols, test_img.rows)));

	int stride = 16;
	int n = padded_img.rows / stride;
	int m = padded_img.cols / stride;
	Rect roi(0, 0, stride * m, stride * n);
	Mat img = padded_img(roi).clone();
	assert(img.isContinuous());

	//        cvtColor(img, img, CV_BGR2GRAY);

	std::cout << "test img size: " << img.rows * img.cols << std::endl << std::endl;

	Timer timer;
	timer.reset();
	auto matches = detector.match(img, ScoreMin_m, ids);
	//timer.out();
	CString strTime(L"");
	strTime.Format("%.2f", timer.elapsed()*1000);
	m_matchTime.SetWindowTextA(strTime);
	if (img.channels() == 1) cvtColor(img, img, CV_GRAY2BGR);

	std::cout << "matches.size(): " << matches.size() << std::endl;
	size_t top5 = matches.size();
	if (top5 > matches.size()) top5 = matches.size();
	//对结果进行筛选
	std::vector<line2Dup::Match> DesMatch;
	bool hasFlag = false;
	for (size_t i = 0; i < top5; i++)
	{
			hasFlag = false;
			if (i == 0)
			{
				DesMatch.push_back(matches[0]);
			}
			for (size_t j = 0; j < DesMatch.size(); j++)
			{
				if (abs(DesMatch[j].x - matches[i].x) < 10 && abs(DesMatch[j].y - matches[i].y) < 10)
				{
					hasFlag = true;
					if (matches[i].similarity > DesMatch[j].similarity)
					{	//当未保存的结果更优时，进行替换
						DesMatch[j] = matches[i];
						break;
					}
				}
			}
			if (!hasFlag)
			{
				DesMatch.push_back(matches[i]);
			}
		
	}
	int TempLateWidth = TemplateImage.cols;
	m_listMsg.DeleteAllItems();
	
	for (size_t i = 0; i < DesMatch.size(); i++) {
		auto match = DesMatch[i];
		auto templ = detector.getTemplates("test",
			match.template_id);

		// 270 is width of template image
		// 100 is padding when training
		// tl_x/y: template croping topleft corner when training

		float r_scaled_width = TemplateImage.cols / 2.0f * infos[match.template_id].scale;

		float r_scaled_height = TemplateImage.rows / 2.0f * infos[match.template_id].scale;

		// scaling won't affect this, because it has been determined by warpAffine
		// cv::warpAffine(src, dst, rot_mat, src.size()); last param
		float train_img_half_width = TemplateImage.cols / 2.0f + 100;
		float train_img_half_height = TemplateImage.rows / 2.0f + 100;

		// center x,y of train_img in test img
		float x = match.x - templ[0].tl_x + train_img_half_width;
		float y = match.y - templ[0].tl_y + train_img_half_height;

		//Msg
		CString str(L"");
		m_listMsg.InsertItem(i, str);
		m_listMsg.SetCheck(i);
		str.Format("%d", i);
		m_listMsg.SetItemText(i, SUBITEM_INDEX, str);
		str.Format("%.2f", DesMatch[i].similarity);
		m_listMsg.SetItemText(i, SUBITEM_SCORE, str);
		str.Format("%.1f", infos[DesMatch[i].template_id].angle);
		m_listMsg.SetItemText(i, SUBITEM_ANGLE, str);
		str.Format("%.3f", x-250);
		m_listMsg.SetItemText(i, SUBITEM_POS_X, str);
		str.Format("%.3f", y-250);
		m_listMsg.SetItemText(i, SUBITEM_POS_Y, str);
		//Msg

		cv::Vec3b randColor;
		randColor[0] = rand() % 155 + 100;
		randColor[1] = rand() % 155 + 100;
		randColor[2] = rand() % 155 + 100;
		for (int i = 0; i < templ[0].features.size(); i++) {
			auto feat = templ[0].features[i];
			cv::circle(img, { feat.x + match.x, feat.y + match.y }, 3, randColor, -1);
		}
		cv::circle(img, Point(x, y), 3, randColor, -1);
		float  ANGLE_M = -infos[match.template_id].angle;
		CString temp_str;
		temp_str.Format("%.1f", ANGLE_M);
		/*cv::putText(img, temp_str.GetBuffer(),
			Point(match.x + r_scaled_width - 100, match.y - 200), FONT_HERSHEY_PLAIN, 2, randColor);
		*/
		
			cv::putText(img, to_string(i),
				Point(match.x + r_scaled_width - 20, match.y - 6), FONT_HERSHEY_PLAIN, 3, randColor);
		cv::RotatedRect rotatedRectangle({ x, y }, { 2 * r_scaled_width, 2 * r_scaled_height }, -infos[match.template_id].angle);
		// cv::RotatedRect rotatedRectangle({ x, y }, { 200, 200}, -infos[match.template_id].angle);

		cv::Point2f vertices[4];
		rotatedRectangle.points(vertices);
		for (int i = 0; i < 4; i++) {
			int next = (i + 1 == 4) ? 0 : (i + 1);
			cv::line(img, vertices[i], vertices[next], randColor, 2);
		}

		std::cout << "\nmatch.template_id: " << match.template_id << std::endl;
		std::cout << "match.similarity: " << match.similarity << std::endl;
	}
	ShowImage(this->GetDlgItem(IDC_STATIC_SrcImage), img);
	/*imshow("img", img);
	waitKey(0);

	std::cout << "test end" << std::endl << std::endl;*/
}
