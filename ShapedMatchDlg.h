﻿#include "opencv2/opencv.hpp"
#include "opencv2/world.hpp"
// ShapedMatchDlg.h: 头文件
//

#pragma once


// CShapedMatchDlg 对话框
/// <summary>
/// 显示图像
/// </summary>
/// <seealso cref="CDialogEx" />
class CShapedMatchDlg : public CDialogEx
{
// 构造
public:
	CShapedMatchDlg(CWnd* pParent = nullptr);	// 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_SHAPEDMATCH_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;
	
	void ShowImage(CWnd* pWnd1,cv::Mat src_img);

	cv::Mat TemplateImage,SrcImage;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	
public:
	afx_msg void OnBnClickedButtonSeltemp();
	afx_msg void OnBnClickedButtonSrc();
	afx_msg void OnBnClickedButton2();
	CEdit TempatePath;
	afx_msg void OnBnClickedButtonMatchtool();
	CEdit m_ScoreMin;
//	CListBox m_listMsg;
	CListCtrl m_listMsg;
	CStatic m_matchTime;
};
